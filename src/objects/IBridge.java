package objects;

import objects.characters.Hero;
import objects.shields.Shield;
import objects.weapons.Weapon;

public interface IBridge {

void ibridge(Hero att, Hero def, Weapon wpnAtt, Weapon wpnDef, Shield shdAtt, Shield shdDef);
}