package objects;

import dice.RollDice;
import objects.characters.Hero;
import objects.weapons.Weapon;
import objects.shields.Shield;

import static java.lang.Math.abs;

public class Calculations {

  /* state */

  /* constructor */

  /* behavior, getters & setters */

  private int getRollDice(int counter) {

    RollDice roll = new RollDice(10);
    int rollDice = roll.rollDice();
    switch (counter) {
      case 1:
        System.out.printf("\nÚtočníkovi padla kocka v prvom ťahu s číslom: %d", rollDice);
        break;
      case 2:
        System.out.printf("\nÚtočníkovi padla kocka v druhom ťahu s číslom: %d", rollDice);
        break;
      case 3:
        System.out.printf("\n\nObrancovi padla kocka v prvom ťahu s číslom: %d", rollDice);
        break;
      default:
        System.out.printf("\nObrancovi padla kocka v druhem ťahu s číslom: %d", rollDice);
        break;
    }
    return rollDice;
  }

  /* setter  */

  /* methods */

  public void meeting(Hero att, Hero def, Weapon wpnAtt, Weapon wpnDef, Shield shdAtt, Shield shdDef) {
    System.out.println("Boj!");

    /* attacker HP = Hero attack Damage + (Hero attack weapon + (Hero attack weapon * random dice number1)) + (Hero attack shield + (Hero attack shield * random dice number2)) */
    int attHP = att.getAttackDmg() + (wpnAtt.getWpnAtt() + (wpnAtt.getWpnAtt() * getRollDice(1) / 10) + shdAtt.getShdAtt() + (shdAtt.getShdAtt() * getRollDice(2)/10));

    /* defender HP = Hero defence Resistance + (Hero defence weapon + (Hero defence weapon * random dice number1)) + (Hero defence shield + (Hero defence shield * random dice number2)) */
    int defHP = def.getDefenceRes() + (wpnDef.getWpnDef() + (wpnDef.getWpnDef() * getRollDice(3) / 10) + shdDef.getShdDef() + (shdDef.getShdDef() * getRollDice(4)/10));

    int resultDefender = def.getHealth();
    int resultCheck = (attHP - defHP);
    int result = resultDefender - resultCheck;

    if (resultCheck >= 0) {
      System.out.printf("\n\nObrancov život sa znížil z pôvodných %d o %d na hodnotu %d%n", resultDefender, resultCheck, result);
      def.setHealth(result);
    } else {
      System.out.printf("\n\nObranca sa ubránil a spôsobil zranenie útočníkovi, ktorému sa život znížil z pôvodných %d o %d na hodnotu %d%n", att.getHealth(), abs(resultCheck), att.getHealth() - abs(resultCheck));
      att.setHealth(att.getHealth() - abs(resultCheck));
    }
  }
}