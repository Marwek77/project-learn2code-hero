package objects.shields;

public class KnightShield extends Shield {

  /* Scutum - rectangular, semi-cylindrical shield used by the army of ancient Rome */

  /* state */

  /* constructor */

  static KnightShield valueOf(int wpnAtt, int wpnDef, String name) {
    return new KnightShield(wpnAtt, wpnDef, name);
  }

  private KnightShield(int shdAtt, int shdDef, String name) {
    super(shdAtt, shdDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Knight shield name: %s%n", name));
    return super.toString();
  }
}