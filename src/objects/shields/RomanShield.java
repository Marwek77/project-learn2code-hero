package objects.shields;

public class RomanShield extends Shield {

  /* Scutum - rectangular, semi-cylindrical shield used by the army of ancient Rome */

  /* state */

  /* constructor */

  static RomanShield valueOf(int wpnAtt, int wpnDef, String name) {
    return new RomanShield(wpnAtt, wpnDef, name);
  }

  private RomanShield(int shdAtt, int shdDef, String name) {
    super(shdAtt, shdDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Roman shield name: %s%n", name));
    return super.toString();
  }
}