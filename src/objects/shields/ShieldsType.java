package objects.shields;

public enum ShieldsType {

  RomanShield, GreekShield, VikingShield, DwarfShield, KnightShield, PikemanShield
}