package objects.shields;

public class VikingShield extends Shield {

  /* Gokstad - large round wooden shield used by Vikings */

  /* state */

  /* constructor */

  static VikingShield valueOf(int wpnAtt, int wpnDef, String name) {
    return new VikingShield(wpnAtt, wpnDef, name);
  }

  private VikingShield(int shdAtt, int shdDef, String name) {
    super(shdAtt, shdDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Shield name: %s%n", name));
    return super.toString();
  }
}