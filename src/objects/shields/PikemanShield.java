package objects.shields;

public class PikemanShield extends Shield {

  /* Scutum - rectangular, semi-cylindrical shield used by the army of ancient Rome */

  /* state */

  /* constructor */

  static PikemanShield valueOf(int wpnAtt, int wpnDef, String name) {
    return new PikemanShield(wpnAtt, wpnDef, name);
  }

  private PikemanShield(int shdAtt, int shdDef, String name) {
    super(shdAtt, shdDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Pikeman shield name: %s%n", name));
    return super.toString();
  }
}