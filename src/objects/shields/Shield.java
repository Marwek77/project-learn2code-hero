package objects.shields;

import objects.IBridge;
import objects.characters.Hero;
import objects.weapons.Weapon;

public abstract class Shield implements IBridge {

  /* state */

  private final int shdAtt;       /* shield attack points */
  private final int shdDef;       /* shield defense points */
  final String name;              /* weapon name */

  /* constructor */

  Shield(int shdAtt, int shdDef, String name) {
    this.shdAtt = shdAtt;
    this.shdDef = shdDef;
    this.name = name;
  }

  /* behavior, getters & setters */

  public int getShdAtt() {
    return shdAtt;
  }

  public int getShdDef() {
    return shdDef;
  }

  public String getName() {
    return name;
  }

  /* methods */

  public String toString() {
    return String.format("shield attack points:\t|%3d| shield defense points:\t|%3d|", shdAtt, shdDef);
  }

  @Override
  public void ibridge(Hero att, Hero def, Weapon wpnAtt, Weapon wpnDef, Shield shdAtt, Shield shdDef) {
    System.out.println("Som v Shield cez ibridge");
  }
}