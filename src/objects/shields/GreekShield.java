package objects.shields;

public class GreekShield extends Shield {

  /* Aspis - heavy wooden shield used by the infantry in various periods of ancient Greece */

  /* state */

  /* constructor */

  static GreekShield valueOf(int wpnAtt, int wpnDef, String name) {
    return new GreekShield(wpnAtt, wpnDef, name);
  }

  private GreekShield(int shdAtt, int shdDef, String name) {
    super(shdAtt, shdDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Shield name: %s%n", name));
    return super.toString();
  }
}