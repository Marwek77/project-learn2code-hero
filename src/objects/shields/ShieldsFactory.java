package objects.shields;

public class ShieldsFactory {

  public static Shield createShield(int shdAtt, int shdDef, String name, ShieldsType type) {

    Shield shield = null;

    switch (type) {
      case RomanShield:
        shield = RomanShield.valueOf(shdAtt, shdDef, name);
        break;
      case GreekShield:
        shield = GreekShield.valueOf(shdAtt, shdDef, name);
        break;
      case VikingShield:
        shield = VikingShield.valueOf(shdAtt, shdDef, name);
        break;
      case DwarfShield:
        shield = DwarfShield.valueOf(shdAtt, shdDef, name);
        break;
      case KnightShield:
        shield = KnightShield.valueOf(shdAtt, shdDef, name);
        break;
      case PikemanShield:
        shield = PikemanShield.valueOf(shdAtt, shdDef, name);
        break;
    }
    return shield;
  }
}