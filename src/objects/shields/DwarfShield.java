package objects.shields;

public class DwarfShield extends Shield {

  /* Scutum - rectangular, semi-cylindrical shield used by the army of ancient Rome */

  /* state */

  /* constructor */

  static DwarfShield valueOf(int wpnAtt, int wpnDef, String name) {
    return new DwarfShield(wpnAtt, wpnDef, name);
  }

  private DwarfShield(int shdAtt, int shdDef, String name) {
    super(shdAtt, shdDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Dwarf shield name: %s%n", name));
    return super.toString();
  }
}