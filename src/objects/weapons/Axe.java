package objects.weapons;

public class Axe extends Weapon {

  /* state */

  /* constructor */

  static Axe valueOf(int wpnAtt, int wpnDef, String name) {
    return new Axe(wpnAtt, wpnDef, name);
  }

  private Axe(int wpnAtt, int wpnDef, String name) {
    super(wpnAtt, wpnDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Axe name: %s", name));
    return super.toString();
  }
}