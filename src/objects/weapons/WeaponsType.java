package objects.weapons;

public enum WeaponsType {

  Sword, Axe, Spear, Hammer, Dagger, Pike
}