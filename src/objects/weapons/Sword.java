package objects.weapons;

public class Sword extends Weapon {

  /* state */

  /* constructor */

  static Sword valueOf(int wpnAtt, int wpnDef, String name) {
    return new Sword(wpnAtt, wpnDef, name);
  }

  private Sword(int wpnAtt, int wpnDef, String name) {
    super(wpnAtt, wpnDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Sword name: %s", name));
    return super.toString();
  }
}