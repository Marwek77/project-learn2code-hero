package objects.weapons;

public class WeaponsFactory {

  public static Weapon createWeapon(int wpnAtt, int wpnDef, String name, WeaponsType type) {

    Weapon weapon = null;

    switch (type) {
      case Sword:
        weapon = Sword.valueOf(wpnAtt, wpnDef, name);
        break;
      case Axe:
        weapon = Axe.valueOf(wpnAtt, wpnDef, name);
        break;
      case Spear:
        weapon = Spear.valueOf(wpnAtt, wpnDef, name);
        break;
      case Hammer:
        weapon = Hammer.valueOf(wpnAtt, wpnDef, name);
        break;
      case Dagger:
        weapon = Dagger.valueOf(wpnAtt, wpnDef, name);
        break;
      case Pike:
        weapon = Pike.valueOf(wpnAtt, wpnDef, name);
        break;
    }
    return weapon;
  }
}