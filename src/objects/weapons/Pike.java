package objects.weapons;

public class Pike extends Weapon {

  /* state */

  /* constructor */

  static Pike valueOf(int wpnAtt, int wpnDef, String name) {
    return new Pike(wpnAtt, wpnDef, name);
  }

  private Pike(int wpnAtt, int wpnDef, String name) {
    super(wpnAtt, wpnDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Pike name: %s", name));
    return super.toString();
  }
}