package objects.weapons;

public class Hammer extends Weapon {

  /* state */

  /* constructor */

  static Hammer valueOf(int wpnAtt, int wpnDef, String name) {
    return new Hammer(wpnAtt, wpnDef, name);
  }

  private Hammer(int wpnAtt, int wpnDef, String name) {
    super(wpnAtt, wpnDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Hammer name: %s", name));
    return super.toString();
  }
}