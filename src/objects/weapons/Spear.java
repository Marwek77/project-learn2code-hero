package objects.weapons;

public class Spear extends Weapon {

  /* state */

  /* constructor */

  static Spear valueOf(int wpnAtt, int wpnDef, String name) {
    return new Spear(wpnAtt, wpnDef, name);
  }

  private Spear(int wpnAtt, int wpnDef, String name) {
    super(wpnAtt, wpnDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Spear name: %s", name));
    return super.toString();
  }
}