package objects.weapons;

public class Dagger extends Weapon {

  /* state */

  /* constructor */

  static Dagger valueOf(int wpnAtt, int wpnDef, String name) {
    return new Dagger(wpnAtt, wpnDef, name);
  }

  private Dagger(int wpnAtt, int wpnDef, String name) {
    super(wpnAtt, wpnDef, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Dagger name: %s", name));
    return super.toString();
  }
}