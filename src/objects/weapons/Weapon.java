package objects.weapons;

import objects.IBridge;
import objects.characters.Hero;
import objects.shields.Shield;

public abstract class Weapon implements IBridge {

  /* state */

  private final int wpnAtt;       /* weapon attack points */
  private final int wpnDef;       /* weapon defense points */
  final String name;              /* weapon name */

  /* constructor */

  Weapon(int wpnAtt, int wpnDef, String name) {
    this.wpnAtt = wpnAtt;
    this.wpnDef = wpnDef;
    this.name = name;
  }

  /* behavior, getters & setters */

  public int getWpnAtt() {
    return wpnAtt;
  }

  public int getWpnDef() {
    return wpnDef;
  }

  public String getName() {
    return name;
  }

  /* methods */

  public String toString() {
    return String.format("weapon attack points:\t|%3d| weapon defense points:\t|%3d|%n", wpnAtt, wpnDef);
  }

  @Override
  public void ibridge(Hero att, Hero def, Weapon wpnAtt, Weapon wpnDef, Shield shdAtt, Shield shdDef) {
    System.out.println("Som vo Weapon cez ibridge");
  }
}