package objects.factories;

public enum FactoriesType {

  HeroesFactory, WeaponsFactory , ShieldsFactory
}