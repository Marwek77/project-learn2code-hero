package objects.factories;

import objects.characters.Hero;
import objects.weapons.Weapon;
import objects.shields.Shield;

public abstract class AbstractFactory {

  public abstract Hero getHero(String hero);
  public abstract Weapon getWeapon(String weapon);
  public abstract Shield getShield(String shield);
}