package objects.characters;

public class Hammerman extends Hero {

  /* state */

  /* constructor */

  static Hammerman valueOf(int health, int attDmg, int defRes, String name) {
    return new Hammerman(health, attDmg, defRes, name);
  }

  private Hammerman(int health, int attDmg, int defRes, String name) {
    super(health, attDmg, defRes, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Hammerman name: %s", name));
    return super.toString();
  }
}