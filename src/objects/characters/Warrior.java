package objects.characters;

public class Warrior extends Hero {

  /* state */

  /* constructor */

  static Warrior valueOf(int health, int attDmg, int defRes, String name) {
    return new Warrior(health, attDmg, defRes, name);
  }

  private Warrior(int health, int attDmg, int defRes, String name) {
    super(health, attDmg, defRes, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Warrior name: %s", name));
    return super.toString();
  }
}