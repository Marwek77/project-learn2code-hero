package objects.characters;

public class Pikeman extends Hero {

  /* state */

  /* constructor */

  static Pikeman valueOf(int health, int attDmg, int defRes, String name) {
    return new Pikeman(health, attDmg, defRes, name);
  }

  private Pikeman(int health, int attDmg, int defRes, String name) {
    super(health, attDmg, defRes, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Pikeman name: %s", name));
    return super.toString();
  }
}