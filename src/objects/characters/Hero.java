package objects.characters;

import objects.Calculations;
import objects.IBridge;
import objects.shields.Shield;
import objects.weapons.Weapon;

public abstract class Hero implements IBridge {

  /* state */

  private int health;             /* health points */
  private final int attDmg;       /* attack damage */
  private final int defRes;       /* defence resistance */
  final String name;              /* Hero name */

  /* constructor */

  Hero(int health, int attDmg, int defRes, String name) {
    this.health = health;
    this.attDmg = attDmg;
    this.defRes = defRes;
    this.name = name;
  }

  /* behavior, getters & setters */

  public int getHealth() {
    return health;
  }

  public int getAttackDmg() {
    return attDmg;
  }

  public int getDefenceRes() {
    return defRes;
  }

  public String getName() {
    return name;
  }

  public void setHealth(int health) {
    this.health = health;
  }

  /* methods */

  private void fight(Hero att, Hero def, Weapon wpnAtt, Weapon wpnDef, Shield shdAtt, Shield shdDef) {

    Calculations calc = new Calculations();

    calc.meeting(att, def, wpnAtt, wpnDef, shdAtt, shdDef);
  }

  public String toString() {
    return String.format("health points:\t|%3d|%nattack damage points:\t|%3d| defense resistance points:|%3d|%n",
            health, attDmg, defRes);
  }

  @Override
  public void ibridge(Hero att, Hero def, Weapon wpnAtt, Weapon wpnDef, Shield shdAtt, Shield shdDef) {
    System.out.println("Som v Hero cez ibridge");
    fight(att, def, wpnAtt, wpnDef, shdAtt, shdDef);
  }
}