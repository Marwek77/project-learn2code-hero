package objects.characters;

public class HeroesFactory {

  public static Hero createHero(int health, int attDmg, int defRes, String name, HeroType type) {

    Hero hero = null;

    switch (type) {
      case Warrior:
        hero = Warrior.valueOf(health, attDmg, defRes, name);
        break;
      case Axeman:
        hero = Axeman.valueOf(health, attDmg, defRes, name);
        break;
      case Spearman:
        hero = Spearman.valueOf(health, attDmg, defRes, name);
        break;
      case Hammerman:
        hero = Hammerman.valueOf(health, attDmg, defRes, name);
        break;
      case Knight:
        hero = Knight.valueOf(health, attDmg, defRes, name);
        break;
      case Pikeman:
        hero = Pikeman.valueOf(health, attDmg, defRes, name);
        break;
    }
    return hero;
  }
}