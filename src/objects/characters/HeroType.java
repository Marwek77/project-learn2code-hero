package objects.characters;

public enum HeroType {

  Warrior, Axeman, Spearman, Hammerman, Knight, Pikeman
}