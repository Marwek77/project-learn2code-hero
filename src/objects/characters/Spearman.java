package objects.characters;

public class Spearman extends Hero {

  /* state */

  /* constructor */

  static Spearman valueOf(int health, int attDmg, int defRes, String name) {
    return new Spearman(health, attDmg, defRes, name);
  }

  private Spearman(int health, int attDmg, int defRes, String name) {
    super(health, attDmg, defRes, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Spearman name: %s", name));
    return super.toString();
  }
}