package objects.characters;

public class Knight extends Hero {

  /* state */

  /* constructor */

  static Knight valueOf(int health, int attDmg, int defRes, String name) {
    return new Knight(health, attDmg, defRes, name);
  }

  private Knight(int health, int attDmg, int defRes, String name) {
    super(health, attDmg, defRes, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Knight name: %s", name));
    return super.toString();
  }
}