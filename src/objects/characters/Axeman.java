package objects.characters;

public class Axeman extends Hero {

  /* state */

  /* constructor */

  static Axeman valueOf(int health, int attDmg, int defRes, String name) {
    return new Axeman(health, attDmg, defRes, name);
  }

  private Axeman(int health, int attDmg, int defRes, String name) {
    super(health, attDmg, defRes, name);
  }

  /* behavior, getters & setters */

  /* methods */

  @Override
  public String toString() {
    System.out.println(String.format("Axeman name: %s", name));
    return super.toString();
  }
}