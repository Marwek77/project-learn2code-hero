package menu;

import java.util.InputMismatchException;
import java.util.Scanner;

class Inputs {

  private final Scanner scanner = new Scanner(System.in);

  /* This class is for reading inputs only */

  int readInteger() {

    /* Numbers reading method universal */

    int number = 0;
    boolean weHaveNumber = false;

    while (!weHaveNumber) {
      try {
        System.out.print("" + "(zadaj svoju voľbu): ");
        number = scanner.nextInt();
        weHaveNumber = true;
      } catch (InputMismatchException ex) {
        System.out.println("Zadaný reťazec nie je celé číslo.");
        System.out.println("Opakujte zadanie!");
        scanner.nextLine();
      }
    }
//    scanner.close();  /* cannot close scanner here otherwise it throw NoSuchElementException, so where? */
    return number;
  }
}