package menu;

import java.util.concurrent.TimeUnit;
import dice.RollDice;

public class MenuMain {

  private int heroStatus;
  private int enemyStatus;

  private int heroWeapon;
  private int enemyWeapon;

  private int heroShield;
  private int enemyShield;

  public int getHeroStatus() {
    return heroStatus;
  }

  private void setHeroStatus(int heroStatus) {
    this.heroStatus = heroStatus;
  }

  public int getEnemyStatus() {
    return enemyStatus;
  }

  private void setEnemyStatus(int enemyStatus) {
    this.enemyStatus = enemyStatus;
  }

  public int getHeroWeapon() {
    return heroWeapon;
  }

  private void setHeroWeapon(int heroWeapon) {
    this.heroWeapon = heroWeapon;
  }

  public int getEnemyWeapon() {
    return enemyWeapon;
  }

  private void setEnemyWeapon(int enemyWeapon) {
    this.enemyWeapon = enemyWeapon;
  }

  public int getHeroShield() {
    return heroShield;
  }

  private void setHeroShield(int heroShield) {
    this.heroShield = heroShield;
  }

  public int getEnemyShield() {
    return enemyShield;
  }

  private void setEnemyShield(int enemyShield) {
    this.enemyShield = enemyShield;
  }

  /* This class is for operations with Menu only */

  private final Inputs inputs = new Inputs();

  /* ------------------- */

  public void menuStart() {

    int status, exit = 3; /* value to end the program */
    do {
      listStart();
      status = inputs.readInteger();
      switch (status) {
        case 1:
          System.out.print("\nVoľba hrdinu:\n"); delayIt(2);
          menuOption1(); delayIt(1);
          System.out.print("\nVoľba zbrane hrdinu:\n"); delayIt(2);
          menuOption11(); delayIt(1);
          System.out.print("\nVoľba štítu hrdinu:\n"); delayIt(2);
          menuOption111(); delayIt(2);
          System.out.print("\nVoľba protivníka:\n"); delayIt(2);
          menuOption2(); delayIt(1);
          System.out.print("\nVoľba zbrane protivníka:\n"); delayIt(2);
          menuOption22(); delayIt(1);
          System.out.print("\nVoľba štítu protivníka:\n"); delayIt(2);
          menuOption222(); delayIt(1);
          break;
        case 2:
          randomSetup();
          break;
        case 3:
          System.out.println();
          System.out.println("Koniec konfigurátora, dovidenia!");
          if (getHeroStatus() == 0 && getEnemyStatus() == 0){
            fixSetup();
          }
          delayIt(1);
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  private void randomSetup() {

    RollDice dice = new RollDice(6);

    setHeroStatus(dice.rollDice());
    setHeroWeapon(dice.rollDice());
    setHeroShield(dice.rollDice());

    setEnemyStatus(dice.rollDice());
    setEnemyWeapon(dice.rollDice());
    setEnemyShield(dice.rollDice());
  }

  private void fixSetup() {

    setHeroStatus(1);
    setHeroWeapon(1);
    setHeroShield(1);

    setEnemyStatus(2);
    setEnemyWeapon(2);
    setEnemyShield(2);
  }

  private void listStart() {

    System.out.println("\n\t\t RPG konfigurátor:\n");
    System.out.println("Voľba 1: Tvoj výber hrdinu a protivníka a ich výzbroje");
    System.out.println("Voľba 2: Náhodný výber hrdinu a protivníka a ich výzbroje");
    System.out.println("Voľba 3: Súboj a výsledky\n");
    System.out.println("Poznámka: Ak si zvolíš voľbu 3 bez výberu hrdinov priamo alebo náhodne, " +
            "tak prebehne automaticky súboj medzi hrdinami Scipio Africanus a Valdemar \n");
  }

  /* Hero */

  private void menuOption1() {

    int status, exit = 0; /* value to end the program */
    do {
      listOptionBoth12();
      System.out.println();
      status = inputs.readInteger();
      switch (status) {
        case 1:
          System.out.println("Tvoja voľba: Warrior\n");
          setHeroStatus(status);
          status = exit;
          break;
        case 2:
          System.out.println("Tvoja voľba: Axeman\n");
          setHeroStatus(status);
          status = exit;
          break;
        case 3:
          System.out.println("Tvoja voľba: Spearman\n");
          setHeroStatus(status);
          status = exit;
          break;
        case 4:
          System.out.println("Tvoja voľba: Hammerman\n");
          setHeroStatus(status);
          status = exit;
          break;
        case 5:
          System.out.println("Tvoja voľba: Knight\n");
          setHeroStatus(status);
          status = exit;
          break;
        case 6:
          System.out.println("Tvoja voľba: Pikeman\n");
          setHeroStatus(status);
          status = exit;
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  private void menuOption11() {

    int status, exit = 0; /* value to end the program */
    do {
      listOptionBoth1122();
      System.out.println();
      status = inputs.readInteger();
      switch (status) {
        case 1:
          System.out.println("Tvoja voľba zbrane: Sword\n");
          setHeroWeapon(status);
          status = exit;
          break;
        case 2:
          System.out.println("Tvoja voľba zbrane: Axe\n");
          setHeroWeapon(status);
          status = exit;
          break;
        case 3:
          System.out.println("Tvoja voľba zbrane: Spear\n");
          setHeroWeapon(status);
          status = exit;
          break;
        case 4:
          System.out.println("Tvoja voľba zbrane: Hammer\n");
          setHeroWeapon(status);
          status = exit;
          break;
        case 5:
          System.out.println("Tvoja voľba zbrane: Dagger\n");
          setHeroWeapon(status);
          status = exit;
          break;
        case 6:
          System.out.println("Tvoja voľba zbrane: Pike\n");
          setHeroWeapon(status);
          status = exit;
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  private void menuOption111() {

    int status, exit = 0; /* value to end the program */
    do {
      listOptionBoth111222();
      System.out.println();
      status = inputs.readInteger();
      switch (status) {
        case 1:
          System.out.println("Tvoja voľba štítu: Roman shield\n");
          setHeroShield(status);
          status = exit;
          break;
        case 2:
          System.out.println("Tvoja voľba štítu: Greek shield\n");
          setHeroShield(status);
          status = exit;
          break;
        case 3:
          System.out.println("Tvoja voľba štítu: Viking shield\n");
          setHeroShield(status);
          status = exit;
          break;
        case 4:
          System.out.println("Tvoja voľba štítu: Dwarf shield\n");
          setHeroShield(status);
          status = exit;
          break;
        case 5:
          System.out.println("Tvoja voľba štítu: Knight shield\n");
          setHeroShield(status);
          status = exit;
          break;
        case 6:
          System.out.println("Tvoja voľba štítu: Pikeman shield\n");
          setHeroShield(status);
          status = exit;
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }


  /* Enemy */

  private void menuOption2() {

    int status, exit = 0; /* value to end the program */
    do {
      listOptionBoth12();
      System.out.println();
      status = inputs.readInteger();
      switch (status) {
        case 1:
          System.out.println("Tvoja voľba: Warrior\n");
          setEnemyStatus(status);
          status = exit;
          break;
        case 2:
          System.out.println("Tvoja voľba: Axeman\n");
          setEnemyStatus(status);
          status = exit;
          break;
        case 3:
          System.out.println("Tvoja voľba: Spearman\n");
          setEnemyStatus(status);
          status = exit;
          break;
        case 4:
          System.out.println("Tvoja voľba: Hammerman\n");
          setEnemyStatus(status);
          status = exit;
          break;
        case 5:
          System.out.println("Tvoja voľba: Knight\n");
          setEnemyStatus(status);
          status = exit;
          break;
        case 6:
          System.out.println("Tvoja voľba: Pikeman\n");
          setEnemyStatus(status);
          status = exit;
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  private void menuOption22() {

    int status, exit = 0; /* value to end the program */
    do {
      listOptionBoth1122();
      System.out.println();
      status = inputs.readInteger();
      switch (status) {
        case 1:
          System.out.println("Tvoja voľba zbrane: Sword\n");
          setEnemyWeapon(status);
          status = exit;
          break;
        case 2:
          System.out.println("Tvoja voľba zbrane: Axe\n");
          setEnemyWeapon(status);
          status = exit;
          break;
        case 3:
          System.out.println("Tvoja voľba zbrane: Spear\n");
          setEnemyWeapon(status);
          status = exit;
          break;
        case 4:
          System.out.println("Tvoja voľba zbrane: Hammer\n");
          setEnemyWeapon(status);
          status = exit;
          break;
        case 5:
          System.out.println("Tvoja voľba zbrane: Dagger\n");
          setEnemyWeapon(status);
          status = exit;
          break;
        case 6:
          System.out.println("Tvoja voľba zbrane: Pike\n");
          setEnemyWeapon(status);
          status = exit;
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  private void menuOption222() {

    int status, exit = 0; /* value to end the program */
    do {
      listOptionBoth111222();
      System.out.println();
      status = inputs.readInteger();
      switch (status) {
        case 1:
          System.out.println("Tvoja voľba štítu: Roman shield\n");
          setEnemyShield(status);
          status = exit;
          break;
        case 2:
          System.out.println("Tvoja voľba štítu: Greek shield\n");
          setEnemyShield(status);
          status = exit;
          break;
        case 3:
          System.out.println("Tvoja voľba štítu: Viking shield\n");
          setEnemyShield(status);
          status = exit;
          break;
        case 4:
          System.out.println("Tvoja voľba štítu: Dwarf shield\n");
          setEnemyShield(status);
          status = exit;
          break;
        case 5:
          System.out.println("Tvoja voľba štítu: Knight shield\n");
          setEnemyShield(status);
          status = exit;
          break;
        case 6:
          System.out.println("Tvoja voľba štítu: Pikeman shield\n");
          setEnemyShield(status);
          status = exit;
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  /* List */

  private void listOptionBoth12() {

    System.out.print("\n\tVoľba 1: Warrior");
    System.out.print("\n\tVoľba 2: Axeman");
    System.out.print("\n\tVoľba 3: Spearman");
    System.out.print("\n\tVoľba 4: Hammerman");
    System.out.print("\n\tVoľba 5: Knight");
    System.out.print("\n\tVoľba 6: Pikeman\n");
  }

  private void listOptionBoth1122() {

    System.out.print("\n\tVoľba 1: Sword");
    System.out.print("\n\tVoľba 2: Axe");
    System.out.print("\n\tVoľba 3: Spear");
    System.out.print("\n\tVoľba 4: Hammer");
    System.out.print("\n\tVoľba 5: Dagger");
    System.out.print("\n\tVoľba 6: Pike\n");
  }

  private void listOptionBoth111222() {

    System.out.print("\n\tVoľba 1: Roman rectangular shield");
    System.out.print("\n\tVoľba 2: Greek heavy wooden shield");
    System.out.print("\n\tVoľba 3: Viking large round wooden shield");
    System.out.print("\n\tVoľba 4: Dwarf shield");
    System.out.print("\n\tVoľba 5: Knight shield");
    System.out.print("\n\tVoľba 6: Pikeman shield\n");
  }

  /* ------------------- */

  private void delayIt(int i) {

    /* Method for delaying messages */

    try {
      TimeUnit.SECONDS.sleep(i);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}