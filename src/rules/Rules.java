package rules;

import menu.MenuMain;
import objects.IBridge;
import objects.characters.*;
import objects.weapons.*;
import objects.shields.*;

import java.util.concurrent.TimeUnit;

import static objects.characters.HeroesFactory.*;
import static objects.shields.ShieldsFactory.*;
import static objects.weapons.WeaponsFactory.*;

public class Rules implements IBridge {

  /* state */

  private Hero attacker;      private Weapon attackerWeapon;      private Shield attackerShield;
  private Hero defender;      private Weapon defenderWeapon;      private Shield defenderShield;

  private int playerMe;       private int playerMeWpn;            private int playerMeShd;
  private int playerHe;       private int playerHeWpn;            private int playerHeShd;

  /* constructor */

  /* behavior */

  private int getPlayerMe() {
    return playerMe;
  }

  private void setPlayerMe(int playerMe) {
    this.playerMe = playerMe;
  }

  private int getPlayerHe() {
    return playerHe;
  }

  private void setPlayerHe(int playerHe) {
    this.playerHe = playerHe;
  }

  private int getPlayerMeWpn() {
    return playerMeWpn;
  }

  private void setPlayerMeWpn(int playerMeWpn) {
    this.playerMeWpn = playerMeWpn;
  }

  private int getPlayerHeWpn() {
    return playerHeWpn;
  }

  private void setPlayerHeWpn(int playerHeWpn) {
    this.playerHeWpn = playerHeWpn;
  }

  private int getPlayerMeShd() {
    return playerMeShd;
  }

  private void setPlayerMeShd(int playerMeShd) {
    this.playerMeShd = playerMeShd;
  }

  private int getPlayerHeShd() {
    return playerHeShd;
  }

  private void setPlayerHeShd(int playerHeShd) {
    this.playerHeShd = playerHeShd;
  }

  /* methods */

  private void menuTransfer() {
    MenuMain menuMain = new MenuMain();
    menuMain.menuStart();

    setPlayerMe(menuMain.getHeroStatus());
    setPlayerMeWpn(menuMain.getHeroWeapon());
    setPlayerMeShd(menuMain.getHeroShield());

    setPlayerHe(menuMain.getEnemyStatus());
    setPlayerHeWpn(menuMain.getEnemyWeapon());
    setPlayerHeShd(menuMain.getEnemyShield());

    System.out.println("\nHrdina: \t" + getPlayerMe() + "\tzbraň " + getPlayerMeWpn() + "\t\tštít " + getPlayerMeShd());
    System.out.println("Protivník: \t" + getPlayerHe() + "\tzbraň " + getPlayerHeWpn() + "\t\tštít " + getPlayerHeShd() + "\n");
  }

  private void data() {

    /* new heroes and weapons can be added here */

    /* testing Inheritance (dedičnosť) in line Hero(Superclass) - Warrior(Subclass) - Axeman(Subclass) - Spearman(Subclass) */

//    Hero character1 = new Warrior(100, 9, 9, "Scipio Africanus");    /* Hero type inside Warrior class */
//    Hero character2 = new Axeman(100, 15, 6, "Valdemar");            /* Hero type inside Axeman class */
//    Hero character3 = new Spearman(100, 13,4,"Donington");           /* Hero type inside Spearman class */

    Hero character1 = createHero(100, 9, 9, "Scipio Africanus", HeroType.Warrior);
    Hero character2 = createHero(100, 15, 6, "Valdemar", HeroType.Axeman);
    Hero character3 = createHero(100, 13,4,"Donington", HeroType.Spearman);
    Hero character4 = createHero(100, 10, 8, "William Wallace", HeroType.Hammerman);
    Hero character5 = createHero(100, 13, 4, "Sir Galahad", HeroType.Knight);
    Hero character6 = createHero(100, 8,12,"Miltiades", HeroType.Pikeman);

    playersHeroConstructor(character1, character2, character3, character4, character5, character6);

    /* testing Inheritance (dedičnosť) in line Weapon(Superclass) - Sword(Subclass) - Axe(Subclass) - Spear(Subclass) */

//    Weapon weapon1 = new Sword(7, 5, "Gladius");      /* passing weapon name */
//    Weapon weapon2 = new Axe(8, 3, "Hel");            /* passing weapon name */
//    Weapon weapon3 = new Spear(9, 1, "Godendart");    /* passing weapon name */

    Weapon weapon1 = createWeapon(7, 5, "Gladius", WeaponsType.Sword);
    Weapon weapon2 = createWeapon(8, 3, "Hel", WeaponsType.Axe);
    Weapon weapon3 = createWeapon(9, 1, "Godendart", WeaponsType.Spear);
    Weapon weapon4 = createWeapon(6, 6, "Mjolnir", WeaponsType.Hammer);
    Weapon weapon5 = createWeapon(6, 2, "Carnwennan", WeaponsType.Dagger);
    Weapon weapon6 = createWeapon(8, 1, "Bardiche",WeaponsType.Pike);

    playersWpnConstructor(weapon1, weapon2, weapon3, weapon4, weapon5, weapon6);

    /* testing Inheritance (dedičnosť) in line Shield(Superclass) - RomanShield(Subclass) - GreekShield(Subclass) - VikingShield(Subclass) */

//    Shield shield1 = new RomanShield(1,6, "Testudo");       /* passing shield name */
//    Shield shield2 = new GreekShield(2, 5, "Hector");       /* passing shield name */
//    Shield shield3 = new VikingShield(2, 5, "Hurstwic");    /* passing shield name */

    Shield shield1 = createShield(1,6, "Testudo", ShieldsType.RomanShield);
    Shield shield2 = createShield(2, 5, "Hector", ShieldsType.GreekShield);
    Shield shield3 = createShield(2, 5, "Hurstwic", ShieldsType.VikingShield);
    Shield shield4 = createShield(1, 5, "Ulthuan", ShieldsType.DwarfShield);
    Shield shield5 = createShield(2, 8, "Escutcheon", ShieldsType.KnightShield);
    Shield shield6 = createShield(2, 6, "Svalinn", ShieldsType.PikemanShield);

    playersShdConstructor(shield1, shield2, shield3, shield4, shield5, shield6);
  }

  private void playersHeroConstructor(Hero character1, Hero character2, Hero character3, Hero character4, Hero character5, Hero character6) {

    switch (getPlayerMe()) {
      case 1:
        this.attacker = character1;
        break;
      case 2:
        this.attacker = character2;
        break;
      case 3:
        this.attacker = character3;
        break;
      case 4:
        this.attacker = character4;
        break;
      case 5:
        this.attacker = character5;
        break;
      default:
        this.attacker = character6;
        break;
    }

    switch (getPlayerHe()) {
      case 1:
        this.defender = character1;
        break;
      case 2:
        this.defender = character2;
        break;
      case 3:
        this.defender = character3;
        break;
      case 4:
        this.defender = character4;
        break;
      case 5:
        this.defender = character5;
        break;
      default:
        this.defender = character6;
        break;
    }
  }

  private void playersWpnConstructor(Weapon weapon1, Weapon weapon2, Weapon weapon3, Weapon weapon4, Weapon weapon5, Weapon weapon6) {

    switch (getPlayerMeWpn()) {
      case 1:
        this.attackerWeapon = weapon1;
        break;
      case 2:
        this.attackerWeapon = weapon2;
        break;
      case 3:
        this.attackerWeapon = weapon3;
        break;
      case 4:
        this.attackerWeapon = weapon4;
        break;
      case 5:
        this.attackerWeapon = weapon5;
        break;
      default:
        this.attackerWeapon = weapon6;
        break;
    }

    switch (getPlayerHeWpn()) {
      case 1:
        this.defenderWeapon = weapon1;
        break;
      case 2:
        this.defenderWeapon = weapon2;
        break;
      case 3:
        this.defenderWeapon = weapon3;
        break;
      case 4:
        this.defenderWeapon = weapon4;
        break;
      case 5:
        this.defenderWeapon = weapon5;
        break;
      default:
        this.defenderWeapon = weapon6;
        break;
    }
  }

  private void playersShdConstructor(Shield shield1, Shield shield2, Shield shield3, Shield shield4, Shield shield5, Shield shield6) {

    switch (getPlayerMeShd()) {
      case 1:
        this.attackerShield = shield1;
        break;
      case 2:
        this.attackerShield = shield2;
        break;
      case 3:
        this.attackerShield = shield3;
        break;
      case 4:
        this.attackerShield = shield4;
        break;
      case 5:
        this.attackerShield = shield5;
        break;
      default:
        this.attackerShield = shield6;
        break;
    }

    switch (getPlayerHeShd()) {
      case 1:
        this.defenderShield = shield1;
        break;
      case 2:
        this.defenderShield = shield2;
        break;
      case 3:
        this.defenderShield = shield3;
        break;
      case 4:
        this.defenderShield = shield4;
        break;
      case 5:
        this.defenderShield = shield5;
        break;
      default:
        this.defenderShield = shield6;
        break;
    }
  }

  private void playersComposition() {
    System.out.printf("%-19s %-18s %-9s %-10s %-9s %-9s %-16s %-6s%n", "This is attacker:", attacker.getName(),
            "weapon:", attackerWeapon.getName(), "shield:", attackerShield.getName(),
            "health status:", attacker.getHealth());
    System.out.printf("%-19s %-18s %-9s %-10s %-9s %-9s %-16s %-6s%n", "This is defender:", defender.getName(),
            "weapon:", defenderWeapon.getName(), "shield:", defenderShield.getName(),
            "health status:", defender.getHealth());
    delayIt(5);
  }

  private void theWinnerTakesItAll() {
    playersComposition();
    System.out.println();
    if (attacker.getHealth() > defender.getHealth()) {
      System.out.printf("%-10s %-20s", "Winner is", attacker.getName());
    } else {
      System.out.printf("%-10s %-20s%n", "Winner is", defender.getName());
    }
  }

  public void rules() {

    menuTransfer();
    data();
    playersComposition();

    int turn = 0; int numberOfTurns = 30;

    do {
      if (turn++ >= numberOfTurns) break;
      if (defender.getHealth() <= 0 || attacker.getHealth() <= 0) {
        break;
      }
      System.out.println();
      System.out.println("*****************************************************************************************");
      System.out.printf("%40sTurn %d: %n", "", turn);
      System.out.println("*****************************************************************************************");
      duel();
    } while (true);

    System.out.println("End of game");
    System.out.println("Koniec v kole " + --turn + "\n");
    theWinnerTakesItAll();
  }

  private void duel() {
    IBridge[] transfer = new IBridge[]{attacker, defender, attackerWeapon, defenderWeapon, attackerShield, defenderShield};
    duelBody(transfer);
  }

  private void duelBody(IBridge[] transfer) {
    IBridge duel;

    duel = transfer[0];
    msg(duel, attacker, defender, attackerWeapon, defenderWeapon, attackerShield, defenderShield);

    duel = transfer[1];
    msg(duel, defender, attacker, defenderWeapon, attackerWeapon, defenderShield, attackerShield);
  }

  private void msg(IBridge duel, Hero attacker, Hero defender, Weapon attackerWeapon, Weapon defenderWeapon, Shield attackerShield, Shield defenderShield) {
    duel.ibridge(attacker, defender, attackerWeapon, defenderWeapon, attackerShield, defenderShield);
    System.out.println("-----------------------------------------------------------------------------------------");
    System.out.println("Attacker " + attacker + attackerWeapon + attackerShield);
    System.out.println("-----------------------------------------------------------------------------------------");
    System.out.println("Defender " + defender + defenderWeapon + defenderShield);
    System.out.println("-----------------------------------------------------------------------------------------");
  }

  private void delayIt(int i) {

    /* Method for delaying messages */

    try {
      TimeUnit.SECONDS.sleep(i);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void ibridge(Hero att, Hero def, Weapon wpnAtt, Weapon wpnDef, Shield shdAtt, Shield shdDef) {    /* connection between classes */
  }
}