import rules.Rules;

class Main {

  public static void main(String[] args) {

    Rules run = new Rules();
    run.rules();
  }
}

/*  created by Marek Kamensky
    public source https://Marwek77@bitbucket.org/Marwek77/project-learn2code-hero.git
    JDK 1.8.0_212, IntelliJ IDEA 2019.1.1
    published on www.learn2code.sk - the task "Naprogramuj hru" - "Program the Game" on 03.05.2019
*/