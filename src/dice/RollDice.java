package dice;

public class RollDice {

  /* state */

  private final int sides;

  /* constructor */

  public RollDice(int sides) {
    this.sides = sides;
  }

  /* behavior, getters & setters */

  public int rollDice() {
    return (int) (Math.random() * sides) + 1;
  }
}